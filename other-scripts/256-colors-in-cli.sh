#!/bin/bash


echo -e "\n\tColors, the old way:\n"

echo -en "\t"
for i in {40..47} {100..107} ; do
	echo -en "\e[${i}m  "
done
echo -e "\e[0m\n\n"

echo -e "\tColors, the '88/256' way:\n"

echo -en "\t"
for i in {0..15} ; do
	echo -en "\e[48;5;${i}m  "
done
echo -e "\e[0m\n\n"

echo -en "\t256 Colors - on full display!\n\n"
#for fgbg in 38 48 ; do # Foreground / Background
fgbg=48 ; echo -en "\t"
    for color in {0..255} ; do # Colors
        # Display the color
        printf "\e[${fgbg};5;%sm  %3s  \e[0m" $color $color
        # Display 6 colors per lines
        if [ $((($color + 1) % 6)) == 4 ] ; then
            echo # New line
            echo -en "\t"
        fi
    done
    echo # New line

#done

echo -en "\t\t"
echo -e " . . . AND . . . \n\n"

echo -e "\tCatch the rainbow! (\e[3mIf you can!\e[0m)\n"
echo -en "\t"
for i in 21 27 33 39 45 51 50 49 48 47 46 82 118 154 190 226 220 214 208 202 196 197 198 199 200 201 165 129 93 57 21 ; do
	echo -en "\e[48;5;${i}m "
done

echo -e "\e[0m\n\n"

