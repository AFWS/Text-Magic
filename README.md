# text-magic 1.0.2b

A utility for manipulating text and text-display - by creating effects through the use of colors (for foreground and background), as well as other style effects (like 'bold-text', 'underline', etc.).

This utility makes it easy for creating neat text-effects to be display in the terminal window.

There is even an option to render the output text with the shell environment attributes escaped (Specifically: so that it can be copy-pasted into another script or project for rendering those text-effects).

This is only a starting point towards a very useful developer's utility.

This script serves as a convenience to the user - in that it greatly simplifies formatting and displaying text, using the shell/terminal environment's own escape sequences - without having the user to try to remember those escape sequences!

